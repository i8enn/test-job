#! /usr/bin/env python3

import datetime
import os
#import sys
import stat
from lib import parseeCSV
from lib import generators

""" Описываем интерфейс взаимодействия с пользователем """
print("\n Добро пожаловать в мини-программу csvToHtml, %s!" % (os.getlogin()))
""" Формируем переменную file_path
в которой будет хранится путь до выбранного файла"""
file_path = '%s/' % (os.getcwd())
""" Узнаем у пользователя путь в до обрабатываемого файла """
while True:
    file_path = '%s/' % (os.getcwd())
    file_path += input(
        "\nУкажите CSV файл который Вы хотите конвертировать: "
        "%s" % (file_path))
    """ Проверяем указывает ли переданный путь на файл и существует ли он"""
    if os.path.isfile(file_path) is True:
        """ Проверяем доступен ли указанный файл для чтения """
        if os.access(file_path, os.R_OK) is True:
            print("\n[OK] Файл %s найден и доступен для чтения \n" % (
                  file_path))
            break
        else:
            print(
                "\n[FAIL] Файл %s не доступен для чтения. Выберите другой "
                "файл или дайте нам права на чтение\n" % (
                    file_path))
    else:
        print(
            "\n[FAIL] %s не является файлом или не существует."
            " Выберите другой файл.\n" % (
                file_path))
    """ Формируем переменную dir_out в которой будет хранится путь
    до каталога в которые будут записываться файлы, а также
    объявляем переменную со стоковым путем вида
    /path/to/script/username/currentdate """
dir_out_default = '%s/output/%s/%s' % (os.getcwd(), os.getlogin(),
                                       datetime.date.today())
print("Так как в этой убогой 0.1-версии ничего не доступно,"
      " Ваш выбор все-равно ни на что не повлияет :|")
dir_out = input("Укажите абсолютный путь до директории, в которую будут "
                "записыватся полученные файлы (по умолчанию %s):\n" % (
                    dir_out_default))
""" Проверяем существует ли директория, есть ли у нас права на запись в нее
и не указал ли пользователь пустую строку => использовать по умолчанию.
Если директория не существует - создаем.
Если у нас нет прав на запись в директории - добавляем.
Если он указал пустую строку (нажал Enter) - используем по умолчанию
"""

""" ЗДЕСЬ ДОЛЖЕН БЫТЬ МЕХАНИЗМ АВТОМАТИЧЕСКОГО СОЗДАНИЯ ДЕРИКТОРИЙ """
""" До его реализации будет использоваться dir_out_default """
dir_out = dir_out_default
os.makedirs(dir_out, 0o777, True)

parseCSV = parseeCSV.CSV(file_path)
dic_csv = parseCSV.parseeCSV()
generate = generators.HTML(dir_out)
generate.generate_html(dic_csv)
