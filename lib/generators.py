import os
from jinja2 import Environment, FileSystemLoader


class HTML():
    """
    Данный класс занимается генерацией HTML файлов
    на основе словаря сгенерированного parseeCSV
    """

    def __init__(self, dir_out):
        super().__init__()
        self.dir_out = dir_out
        #: Определяет место расположения Jinja2 шаблонов в файловой системе
        self.TEMPLATE_ROOT = os.path.join(
            os.path.dirname(__file__), 'templates')
        #: Определяет директорию для сохранения сгенерированных модулей
        #: Стандартная директория
        #self.OUTPUT_ROOT = os.path.join(os.path.dirname(__file__), 'reports')
        #: Кастомная директория
        self.OUTPUT_ROOT = dir_out
        print(self.OUTPUT_ROOT)
        #: Среда выполнения шаблонов Jinja2
        self.jinja_env = Environment(loader=FileSystemLoader(
            self.TEMPLATE_ROOT))

    def generate_html(self, data, template='', outfile=''):
        """
        Метод выполняет генерацию модуля на основе указанного шаблона
        :param data: данные для рендеринга шаблона;
        :param template: файл шаблона, используемый для генерации отчета;
        :param outfile: имя файла для сохранения сгенерированного кода
                        относительно директории :data:`OUTPUT_ROOT`;
        """
        """code = self.jinja_env.get_template(template).render(**data)
        with open(os.path.join(self.OUTPUT_ROOT, outfile), 'w') as f:
            f.write(code.encode('utf-8'))"""
        select_keys = [item for item in data]
        
        html_links = []
        for tmp in select_keys:
            for tmp1 in tmp.keys():
                """ Создание дочерних страниц """
                files = tmp[tmp1].keys()
                for content in tmp[tmp1]:
                    data = {'title': 'Site information',
                            'header_1': 'Popular pages',
                            'contents': tmp[tmp1][content].values()}
                    template = self.jinja_env.get_template('key_template')
                    for file in files:
                        with open("%s/%s.html" % (self.OUTPUT_ROOT,
                                                  file),
                                  "w") as f:
                            f.write(template.render(data))
                """ Создание индексной страницы """
                html_links.append({name: 'href="%s.html"' % (name) for name
                                   in tmp[tmp1]})
                data = {'title': 'Site information',
                        'header_1': 'Popular pages',
                        'links_data': html_links}
                print(html_links)
        template = self.jinja_env.get_template('index_template')
        with open("%s/index.html" % (self.OUTPUT_ROOT), "w") as f:
            f.write(template.render(data))
