import csv


class CSV():
    """Данный класс занимается парсом и приобразованием файла CSV
    Принимает путь к CSV файлу"""

    def __init__(self, file_path):
        super().__init__()
        self.file_path = file_path
        self.csv_dict = {}

    def parseeCSV(self):
        """Данный метод парсит переданный в класс CSV файл"""
        print('\n')
        self.getDictCSV()
        select_keys = self.getSelectKeysFromUsers(self.getListKeysCSV())
        unic_key = self.getDictUnicValFromCustomKeys(select_keys)
        sort_csv = self.getCsvSortForDictKeys(unic_key)
        return sort_csv

    def getDictCSV(self):
        """
        Данный метод создает словарь из переданного в класс CSV файла
        Словарь вида {Номер строки(записи){строка(запись)}}
        """
        i = 0
        with open(self.file_path, 'r') as csv_desctiptor:
            csv_obj = csv.DictReader(csv_desctiptor)
            for csv_dict in csv_obj:
                self.csv_dict[i] = csv_dict
                i = i + 1
        del csv_desctiptor
        return True

    def getListKeysCSV(self):
        """Данный метод возвращает все ключи из словаря self.csv_dict[0]"""
        return dict.keys(self.csv_dict[0])

    def getDictUnicValFromCustomKeys(self, keys):
        """
        Данный метод позволяет передать словарь ключей
        в etUnicValFromCustomKeys
        """
        return {key: self.getUnicValFromCustomKeys(key) for key in keys}

    def getUnicValFromCustomKeys(self, keys):
        """Данный метод возвращает уникальные значения по выбранным полям"""
        i = 0
        val_key = {}
        while i < len(self.csv_dict):
            val_key[i] = self.csv_dict[i].get(keys)
            unic_val_key = set(val_key.values())
            i = i + 1
        return unic_val_key

    def getCsvSortForDictKeys(self, dict_k):
        """
        Данный метод позволяет передать словарь ключей
        в getCsvSortForKeys
        """
        csv_dict_for_keys = []
        for keys in dict_k:
            select_keys = keys
            keys_dict = dict_k.get(keys)
            csv_dict_for_keys.append(self.getCsvSortForKeys(
                keys_dict, select_keys))
        return csv_dict_for_keys

    def getCsvSortForKeys(self, keys, select_keys):
        """
        Данный метод формирует словарь для последующего пре-рендеринга.
        В соответствии с переданным словарем выбраных ключей и названием ключа
        он "проходит" по словарю с CSV и формирует новый словарь вида:
        csv_dict[key_select][key_select_val][num-element]
        [key_element][value_element]
        Например:
        csv_dict{'zip'}{'95838'}{1}{'longitude': '-121.08434', 'sq__ft': '0',
        'zip': '95603', 'latitude': '38.891935', 'street': '1740 HIGH ST',
        'price': '504000', 'state': 'CA', 'city': 'AUBURN', 'baths': '3',
        'beds': '3', 'sale_date': 'Tue May 20 00:00:00 EDT 2008',
        'type': 'Residential'}
        """
        return {select_keys: {rows: {j: item for j, item in enumerate(self.
                csv_dict.values()) if item.get(select_keys) == rows} for rows
            in keys}}

    """Данный метод спрашивает у пользователя
    по каким полям делать сортировку и выборку
    Пока работает очень коряво:
    Не работают числа 2+ разряда
    Не правильно сопоставляются введеные числа и
    ключи списка"""
    def getSelectKeysFromUsers(self, keys_dict):
        while True:
            print('В выбранном Вами файле есть следующие ключи:')
            i = 1
            keys = []
            selected_users_key = []
            for key in keys_dict:
                keys.append(key)
                print('%d - %s' % (i, key))
                i = i + 1
            select = input('Выбирете цифру или несколько цифр без разделителя,'
                           'соотвтетствующие ключам, которые Вы хотите выбрать'
                           '\n(по-умолчанию это будет zip и city)\n'
                           'Пример выбора пунктов 1, 3, 6: 136 \n'
                           'Выбирайте ;):\n'
                           '! Увы, но у Вас ничего не получится '
                           'на версии 0.1 :|\n'
                           'Так что жмите Enter '
                           'и наслаждайтесь автоматизмом :)')
            """
            Код привиденный ниже нужно доработать в следующем минорном релизе,
            а пока мы его закомментируем ;)
            """
            """parce_select = [int(number) for number in select if
                            '0' <= select <= '9']
            for selected_key in parce_select:
                selected_users_key.append(keys[selected_key]
            if selected_users_key == []:
            """
            selected_users_key = ['zip', 'city']
            if input('Вы выбрали: %s. Продолжить?'
                     ' [Yes/no] ' % (selected_users_key)) == 'Yes':
                return selected_users_key
                break
            else:
                print('\n=================================================\n'
                      'Вы решили повторить процедуру :)\nНу что же, '
                      'будем выбирать пока вы не согласитесь продолжить...'
                      '\n=================================================\n')
